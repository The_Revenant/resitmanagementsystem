<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>Resit/Special Exam Registration</title>
    <link rel="stylesheet" href="bootstrap-3.3.7-dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="bootstrap-3.3.7-dist/font-awesome-4.7.0/font-awesome-4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="bootstrap-3.3.7-dist/css/custom.css">
</head>
<%
    String msg = (String) request.getAttribute("msg");
%>
<body>
<div class="container" style="margin-top: 100px;">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="panel panel-default">
                <h2>EGERTON UNIVERSITY RESIT MANAGEMENT SYSTEM</h2>
                <br>
                <br>
                <div class="panel-body">
                    <form action="login" method="post">
                        <div class="form-group">
                            <div class="alert alert-danger" role="alert">
                                <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"
                                      style="align-content: center"></span>
                                <span class="sr-only">Error:</span> <%=msg%>
                            </div>
                            <input type="text" name="userName" class="form-control"
                                   placeholder="Enter Student Number..."
                                   required>
                        </div><!-- form group-->
                        <div class="form-group">
                            <input type="password" name="userPass" class="form-control"
                                   placeholder="Enter  password...."
                                   required>
                        </div><!-- form group-->
                        <div class="form-group">
                            <input type="submit" name="submit" class="btn btn-success btn-lg btn-block" value="Login">
                        </div><!-- form group-->
                    </form>
                </div><!-- panel-body -->
            </div><!-- Panel default -->
        </div><!-- col -->
    </div><!-- row -->
</div><!-- container -->
</body>
</html>
