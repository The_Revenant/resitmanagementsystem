<%--
  User: O3
  Date: 1/17/2018
  Time: 2:10 PM
--%>
<%@ page contentType="text/html;charset=UTF-8" %>
<jsp:useBean id="student" class="rms.model.StudentBean" scope="session"/>
<jsp:setProperty name="student" property="*"/>
<html>
<head>
    <title>Resit/Special Exams Registration</title>
</head>
<body>
<h3>Welcome</h3>
<p>
    Registration Number:
    <jsp:getProperty name="student" property="userName"/>
    <br>
    Name:
    <jsp:getProperty name="student" property="fullName"/>
    <br>
    Gender:
    <jsp:getProperty name="student" property="gender"/>
    <br>
    Faculty:
    <jsp:getProperty name="student" property="faculty"/>
    <br>
    Department:
    <jsp:getProperty name="student" property="department"/>
    <br>
    Course:
    <jsp:getProperty name="student" property="course"/>
    <br>
    Year of Study:
    <jsp:getProperty name="student" property="yearOfStudy"/>
</p>
</body>
</html>
