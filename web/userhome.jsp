<%--
  User: O3
  Date: 1/17/2018
  Time: 2:53 AM
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:useBean id="user" class="rms.model.UserBean" scope="page"/>
<jsp:setProperty name="student" property="*"/>
<html>
<head>
    <title>Resit Management System</title>
</head>
<body>
<h3>You have successfully logged in</h3>
<p>
    Registration:
    <jsp:getProperty name="student" property="userName"/>
    <br>
</p>
</body>
</html>
