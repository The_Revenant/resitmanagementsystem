package rms.model;

import java.io.Serializable;

public class UserBean implements Serializable {
    private String userName;
    private String userPass;

    public UserBean() {

    }

    public UserBean(String uName, String uPass) {
        userName = uName;
        userPass = uPass;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserPass() {
        return userPass;
    }

    public void setUserPass(String userPass) {
        this.userPass = userPass;
    }
}
