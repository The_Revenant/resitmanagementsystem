package rms.model;

public class StudentBean extends UserBean {
    private String fullName;
    private String gender;
    private String faculty;
    private String department;
    private String course;
    private int yearOfStudy;

    public StudentBean() {

    }

    public StudentBean(String username, String password, String fullName, String gender, String faculty, String department, String course, int yearOfStudy) {
        this.setUserName(username);
        this.fullName = fullName;
        this.gender = gender;
        this.faculty = faculty;
        this.department = department;
        this.course = course;
        this.yearOfStudy = yearOfStudy;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getFaculty() {
        return faculty;
    }

    public void setFaculty(String faculty) {
        this.faculty = faculty;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getCourse() {
        return course;
    }

    public void setCourse(String course) {
        this.course = course;
    }

    public int getYearOfStudy() {
        return yearOfStudy;
    }

    public void setYearOfStudy(int yearOfStudy) {
        this.yearOfStudy = yearOfStudy;
    }
}
