package rms.control;

import rms.model.StudentBean;
import rms.services.UserDA;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "Register")
public class Register extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        StudentBean newStudent = new StudentBean(request.getParameter("regNo"), request.getParameter("userPass"), request.getParameter("fullName"), request.getParameter("gender"), request.getParameter("faculty"), request.getParameter("department"), request.getParameter("course"), Integer.parseInt(request.getParameter("yearOfSTudy")));
        if (UserDA.registerStudent(newStudent)) {
            request.setAttribute("msg", newStudent.getUserName() + " registered successfully");
            String url = "/loginmsg.jsp";
            getServletContext().getRequestDispatcher(url).forward(request, response);
        } else {
            request.setAttribute("msg", newStudent.getUserName() + " not registered");
            String url = "/loginmsg.jsp";
            getServletContext().getRequestDispatcher(url).forward(request, response);
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
