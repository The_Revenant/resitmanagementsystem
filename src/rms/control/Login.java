package rms.control;

import rms.model.UserBean;
import rms.services.DatabaseAccess;
import rms.services.UserDA;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "Login")
public class Login extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        UserBean loggingIn = new UserBean(request.getParameter("userName"), DatabaseAccess.hashPassword(request.getParameter("userPass")));
        DatabaseAccess.createConnection();
        if (UserDA.attemptLogin(loggingIn)) {
            String userType = UserDA.getUserType(loggingIn.getUserName());
            String url;
            switch (userType) {
                case "STUDENT":
                    request.setAttribute("student", UserDA.retrieveStudent(loggingIn.getUserName()));
                    url = "/studenthome.jsp";
                    getServletContext().getRequestDispatcher(url).forward(request, response);
                    break;
                default:
                    request.setAttribute("user", loggingIn);
                    url = "/userhome.jsp";
                    getServletContext().getRequestDispatcher(url).forward(request, response);
            }
        } else {
            request.setAttribute("msg", "Invalid details or non-existent user");
            String url = "/loginmsg.jsp";
            getServletContext().getRequestDispatcher(url).forward(request, response);
            DatabaseAccess.closeConnection();
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
