package rms.services;

import rms.model.StudentBean;
import rms.model.UserBean;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UserDA {
    public static boolean attemptLogin(UserBean user) {
        try {
            boolean login = false;
            PreparedStatement statement = DatabaseAccess.connection.prepareStatement("SELECT username FROM users WHERE username=? AND password=?");
            statement.setString(1, user.getUserName());
            statement.setString(2, user.getUserPass());
            ResultSet rs = statement.executeQuery();
            while (rs.next()) {
                login = true;
            }
            return login;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    /*public static boolean signUp(UserBean user){
        try{
            boolean signUp = false;
            PreparedStatement statement=DatabaseAccess.connection.prepareStatement("INSERT INTO users (username, password, name, faculty, department, course, yearOfStudy) VALUES (?,?,?,?,?,?,?)");
            statement.setString();
        }catch(SQLException e){
            throw new RuntimeException(e);
        }
    }*/
    public static String getUserType(String userName) {
        try {
            PreparedStatement statement = DatabaseAccess.connection.prepareStatement("SELECT type FROM users WHERE username = ?");
            statement.setString(1, userName);
            ResultSet rs = statement.executeQuery();
            rs.next();
            return rs.getString(1);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public static StudentBean retrieveStudent(String userName) {
        try {
            PreparedStatement statement = DatabaseAccess.connection.prepareStatement("SELECT name, gender, faculty, department, course, yearOfStudy FROM users WHERE username = ?");
            statement.setString(1, userName);
            ResultSet rs = statement.executeQuery();
            rs.next();
            return new StudentBean(userName, null, rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getString(5), rs.getInt(6));
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public static boolean registerStudent(StudentBean student) {
        try {
            PreparedStatement statement = DatabaseAccess.connection.prepareStatement("INSERT INTO users (username, password, type, name, gender, faculty, department, course, yearOfStudy) VALUES (?, ?, 'STUDENT', ?, ?, ?, ?, ?, ?)");
            statement.setString(1, student.getUserName());
            statement.setString(2, student.getUserPass());
            statement.setString(3, student.getFullName());
            statement.setString(4, student.getGender());
            statement.setString(5, student.getFaculty());
            statement.setString(6, student.getDepartment());
            statement.setString(7, student.getCourse());
            statement.setInt(8, student.getYearOfStudy());
            return statement.execute();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
